module.exports = {
  publicPath: process.env.NODE_ENV === 'production'
  ? '/Development_prod/'
  : '/'
}
